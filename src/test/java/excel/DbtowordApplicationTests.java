package excel;

import com.github.stupdit1t.excel.Column;
import com.github.stupdit1t.excel.ExcelUtils;
import com.github.stupdit1t.excel.style.CellPosition;
import com.github.stupdit1t.excel.style.DefaultCellStyleEnum;
import com.github.stupdit1t.excel.style.ICellStyle;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.poi.ss.usermodel.*;
import org.junit.Test;
import util.JdbcUtil;
import util.JsonBinder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;


class DbtowordApplicationTests {
    private static final Gson jsonBinder = JsonBinder.buildNormalBinderAndHandlerMap("yyyy-MM-dd HH:mm:ss");
    static String[] yw = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public static void main(String[] args)  throws SQLException {

        ICellStyle titleStyle = new ICellStyle() {
            @Override
            public CellPosition getPosition() {
                // 样式位置
                return CellPosition.TITLE;
            }

            @Override
            public void handleStyle(Font font, CellStyle style) {
                font.setFontHeightInPoints((short) 15);
                font.setColor(IndexedColors.RED.getIndex());
                font.setBold(true);
                // 左右居中
                style.setAlignment(HorizontalAlignment.CENTER);
                // 上下居中
                style.setVerticalAlignment(VerticalAlignment.CENTER);
                style.setFont(font);
            }
        };

        QueryRunner qr = new QueryRunner();
        Connection pgsqlConnect = JdbcUtil.getPgsqlConnect("postgres", "123456", "172.16.100.155", "5432", "qgzhdc-zs", "public");
        List<Map<String, Object>> query = qr.query(pgsqlConnect, "select * from config_field_info where ssb = 'zrzhczt_ggfwss_xx' and isxt = '1' and (sfdtpzzd = '1' or  (sfqczd = '1' and sfdtpzzd = '1'))  order by sfqczd desc ,px ", new MapListHandler());
        Map<String, String> headerRules = new HashMap<>();
        int size = query.size();
        Column[] column = new Column[size];
        for (int j = 0; j < size; j++) {
            Map<String, Object> field = query.get(j);

            //是否包含其他说明
            String isvalid =String.valueOf(field.get("isvalid"));
            //字段类型（0枚举字段1填写字段）
            String zdlx =String.valueOf(field.get("zdlx"));
//            指标解释
            String  zbjs =String.valueOf(field.get("zbjs"));
            // 枚举项列表
            String  mjlb =String.valueOf(field.get("mjlb"));
            // 枚举项类型（1单选2多选）
            String  mjxlx =String.valueOf(field.get("mjxlx"));
            // 是否清查字段 0否1是
            String  sfqczd =String.valueOf(field.get("sfqczd"));
            // 字段面板值
            String  zdmbz =String.valueOf(field.get("zdmbz"));
            // 字段面板值
            String  zddw =String.valueOf(field.get("zddw"));
            String key = "1,1," + (j >= 26  ?yw[j / 26-1]+""+yw[j % 26]:yw[j % 26] )+ "," +(j >= 26  ?yw[j / 26-1]+""+yw[j % 26]:yw[j % 26] );
            headerRules.put(key, zdmbz);
            String key1 = "2,2," + (j >= 26 ?yw[j / 26-1]+""+yw[j % 26]:yw[j % 26] ) + "," + (j >= 26 ?yw[j / 26-1]+""+yw[j % 26]:yw[j % 26] );
            headerRules.put(key1, zddw);
            Column c ;
            System.out.println(zdmbz);
            if ("0".equalsIgnoreCase(zdlx)){
                ArrayList<String> mjlbList = jsonBinder.fromJson(mjlb, new TypeToken<ArrayList<String>>() {
                }.getType());
                if("2".equalsIgnoreCase(mjxlx)){
                    String[] xllb =new String[mjlbList.size()];
                    for (int i = 0; i <mjlbList.size() ; i++) {
                        xllb[i] = mjlbList.get(i);
                    }
                    c = Column.field(zdmbz).dorpDown(xllb);
                }else{
                    String s = mjlbList.toString().replaceAll("\"", "").replaceAll(",", ";").replaceAll("\\[", "").replaceAll("]", "");
                    String[] xllb = {s};
                    c = Column.field(zdmbz).dorpDown(xllb);
                }
            }else{
                c = Column.field(zdmbz);
            }
            c.postil(zbjs).backColor(IndexedColors.BLUE);
            column[j] = c;

        }
// 4.导出规则定义
        ExcelUtils.ExportRules exportRules = ExcelUtils.ExportRules.complexRule(column, headerRules)
                // 全局表格样式定义, 默认就是这个设置, 可自定义, 如果自定义则以自定义为主
                .globalStyle(DefaultCellStyleEnum.TITLE, DefaultCellStyleEnum.HEADER, DefaultCellStyleEnum.CELL);

// 3.执行导出到工作簿
        Workbook bean = ExcelUtils.createWorkbook(Collections.emptyList(), exportRules, true);

// 4.写出文件
        try {
            bean.write(new FileOutputStream("C:\\Users\\admin\\Desktop\\应急11项-20201228\\模板测试\\test.xlsx"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
